.. lrdgdal
   (C) 2014 Thomas Scharrenbach
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at
..
        http://www.apache.org/licenses/LICENSE-2.0
..
   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

Quickstart
==========

Installation
------------

::

  pip install lrdgdal


Usage
-----

Let's start with loading a raster from the Web.
::

  from lrdgdal import *

  d =  RasterRDFDataset()
  d.parse(format='gdal', location='ftp://ftp.remotesensing.org/pub/geotiff/samples/gdal_eg/cea.tif',
      publicID='http://example.com/myRaster/')


Now let's check the pixel value of a certain point. First we encode a point in
WKT notation into a valid RDF WKTLiteral as defined by GeoSPARQL.

::

  for row in d.query('''PREFIX gdal: <http://scharrenbach.net/gdal#>
      PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
      SELECT *  WHERE {
        { ?band a gdal:Band . }
        Graph ?g {
          ?band gdal:bandValue ?band_val .
          ?band_val gdal:pixelCoordinates
             "<urn:ogc:def:crs:EPSG::4326> POINT( -117.64108514464233 33.94328904956703 )"^^<http://www.opengis.net/geosparql#wktLiteral>.
          ?band_val gdal:pixelValue ?pixelValue .
          }
      }
      '''):
      print('Band: {} Value {}'.format(row['band'], row['pixelValue']))


The above query shows no results! Why? Well, we forgot to active the gdal SPARQL extension:

::

  from lrdgdal.namespace import *
  sparql.activate(modules=[GDAL.band_value])


If we repeat the query, we should be able to obtain the following result:

::

  Band: http://example.com/raster/band1/ Value 0


Since our example raster image had just one band, we obtain its only band. It happens that the value of the specified
point is zero.