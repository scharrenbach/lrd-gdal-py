.. lrdgdal
   (C) 2014 Thomas Scharrenbach
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at
..
        http://www.apache.org/licenses/LICENSE-2.0
..
   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

Examples
========

Clipping
--------

We want to extract the read rectangle from the picture below.


.. figure:: images/input_with_clip.png
   :figwidth: 50 %
   :alt: part of original picture with clipped area
   :target: http://download.osgeo.org/geotiff/samples/misc/tjpeg.tif

   Part of original picture with clipped area (click to download).

::

  import math
  from osgeo import gdal
  import lrdgdal
  from lrdgdal import gdalutils, gdalrdf
  from lrdgdal import *

  # Replace this with the path to your raster file
  source_raster_file = '/tmp/test.tif'
  clipped_raster_file = '/tmp/result.tif'

  # Load a raster from a file
  src_ds = gdalrdf.create_raster_from_file(filename=source_raster_file)

  # Dummy IRI for creating a Raster object.
  raster_iri = 'http://example.com/raster/'

  r = Raster(dataset=src_ds, raster_iri=raster_iri)

  # Let's take the raster's bounding box and rotate and scale it.
  # This will give us a list of points which we will project to a proper
  # spatial reference system. We will create a polygon from these points
  # which we then use as a mask for clipping the raster.
  mask = r.bbox
  phi = math.radians(5)
  for p in mask:
      # change scale
      p[0] *= 0.5
      p[1] *= 0.25
      # rotation
      p[0] = p[0]*math.cos(phi) + p[1]*math.sin(phi)
      p[1] = - p[0]*math.sin(phi) + p[1]*math.cos(phi)
      # translation
      p[0] -= 50
      p[1] += 250
      pass

  # Project the mask (list of points) from raster coordinates into WGS84
  mask = gdalutils.project_raster_to_geo(
      points=mask,
      affine_trans=r.affine_trans)

  # Turn the list of points into a GDAL polygon.
  mask = gdalutils.points2polygon(mask)

  # Clip the raster with the polygon.
  result = gdalutils.clip_dataset(r.dataset, mask)

  # Save the result to disk
  gdal_driver_name = 'GTiff'
  gdal_driver = gdal.GetDriverByName(gdal_driver_name)
  result_final = gdal_driver.CreateCopy(clipped_raster_file, result, 0)

  # Flush the raster to disk
  result = None
  result_final = None

