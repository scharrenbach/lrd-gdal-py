.. lrdgdal
   (C) 2014 Thomas Scharrenbach
   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at
..
        http://www.apache.org/licenses/LICENSE-2.0
..
   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

Examples SPARQL
===============

Clipping
--------

We want to extract the read rectangle from the picture below.


.. figure:: images/input_with_clip.png
   :figwidth: 50 %
   :alt: part of original picture with clipped area
   :target: http://download.osgeo.org/geotiff/samples/misc/tjpeg.tif

   Part of original picture with clipped area (click to download).

::

  import math
  import codecs
  from osgeo import gdal, osr
  import lrdgdal
  from lrdgdal import gdalutils, gdalrdf
  from lrdgdal import *
  from lrdgdal.namespace import GDAL, GEOSPARQL
  from rdflib import Literal
  from rdflib.namespace import RDF, XSD

  # Replace this with the path to your raster file
  source_raster_file = '/tmp/test.tif'
  clipped_raster_file = '/tmp/result.tif'

  # Load a raster from a file
  src_ds = gdalrdf.create_raster_from_file(filename=source_raster_file)

  # Dummy IRI for creating a Raster object.
  raster_iri = 'http://example.com/raster/'

  r = Raster(dataset=src_ds, raster_iri=raster_iri)

  # Let's take the raster's bounding box and rotate and scale it.
  # This will give us a list of points which we will project to a proper
  # spatial reference system. We will create a polygon from these points
  # which we then use as a mask for clipping the raster.
  mask = r.bbox
  phi = math.radians(5)
  for p in mask:
      # change scale
      p[0] *= 0.5
      p[1] *= 0.25
      # rotation
      p[0] = p[0]*math.cos(phi) + p[1]*math.sin(phi)
      p[1] = - p[0]*math.sin(phi) + p[1]*math.cos(phi)
      # translation
      p[0] -= 50
      p[1] += 250
      pass

  # Project the mask (list of points) from raster coordinates into WGS84
  mask = gdalutils.project_raster_to_geo(
      points=mask,
      affine_trans=r.affine_trans)

  # Turn the list of points into a GDAL polygon.
  mask = gdalutils.points2polygon(mask)

  # Load the raster as an RDF dataset.
  g = RasterRDFDataset()
  g.add_raster(r)

  # Clip the raster with the polygon.
  mask_geometry_literal_value = ogcutils.encode_as_wktliteral(
      spatial_ref=osr.SpatialReference(r.dataset.GetProjection()), geometry=mask)
  mask_geometry_literal = Literal(mask_geometry_literal_value, datatype=GEOSPARQL.WKT_LITERAL)

  query = \
      'PREFIX gdal: <{prefix_gdal}>\n' \
      'PREFIX rdf: <{prefix_rdf}>\n' \
      'SELECT *  WHERE {{\n' \
      '{{\n' \
      '  ?band a gdal:Band .\n' \
      '}}\n' \
      'Graph ?g {{\n' \
      '  ?band gdal:clipValue ?clip_val .\n' \
      '  ?clip_val gdal:clip ?clip .\n' \
      '  ?clip_val gdal:maskGeometry {mask_geometry} .\n' \
      '}}\n' \
      '}}'\
      .format(
          prefix_gdal=GDAL.NS,
          prefix_rdf=RDF.uri,
          mask_geometry=mask_geometry_literal.n3())

  # Activate the clip bgp extension.
  sparql.activate(modules=[GDAL.clip])

  # Save the result to disk
  for row in g.query(query):
      result = codecs.decode(row['clip'].value, 'hex_codec')
      with open(clipped_raster_file, 'w+b') as result_file:
          result_file.write(result)

