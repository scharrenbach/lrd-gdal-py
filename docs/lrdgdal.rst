lrdgdal package
===============

Subpackages
-----------

.. toctree::

    lrdgdal.namespace

Submodules
----------

.. toctree::

   lrdgdal.gdalutils
   lrdgdal.ogcutils
   lrdgdal.sparql

Module contents
---------------


.. autofunction:: lrdgdal.create_raster_from_file

.. autofunction:: lrdgdal.create_raster_from_stream

.. autofunction:: lrdgdal.create_raster_from_data

.. autofunction:: lrdgdal.create_raster_from_iri

.. autofunction:: lrdgdal.get_pixel_value

.. autoclass:: lrdgdal.RasterRDFDataset
   :members:

.. autoclass:: lrdgdal.Raster
   :members:

.. autoclass:: lrdgdal.BandRDFGraph
   :members:

