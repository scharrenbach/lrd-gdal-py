#!/usr/bin/env python
# coding:utf-8

"""lrdgdal tests for the gdalrdf module"""

__author__ = "Thomas Scharrenbach (thomas@scharrenbach.net)"
__copyright__ = "Copyright (C) 2014 Thomas Scharrenbach"
__license__ = "Apache License v2"
__version__ = "0.0.4"

import sys

import traceback

import os

import math

import unittest

import logging

import logging.config

from lrdgdal import *

from rdflib import URIRef

try:
    from osgeo import gdal, osr, ogr
except ImportError:
    import gdal
    import osr
    import ogr

import tempfile

from tests.testutils import TestLRDGDAL

logging.config.fileConfig('tests/logging.conf')

gdal.UseExceptions()

class TestOpClip(TestLRDGDAL):
    """Test clipping."""

    @classmethod
    def setUpClass(cls):
        cls.test_output = 'clip_dataset'
        cls.init_geotiff_files()

    def test_clip(self):
        ogr_driver_name = 'Memory'

        try:
            for root, geotiff_file in self.geotiff_files:
                src_ds = create_raster_from_file(filename=os.path.join(root, geotiff_file))
                logging.info('Started clipping for file: "{}" ...'.format(geotiff_file))

                raster_iri = 'http://example.com/raster/'
                r = Raster(
                    dataset=src_ds,
                    raster_iri=raster_iri)

                other = r.bbox
                logging.debug(other)
                rot = math.radians(5)
                for p in other:
                    #p[0] = p[0] * 0.5 + p[1] * 0.5
                    #p[1] = p[0] * 0.5 + p[1] * 0.5
                    p[0] *= 0.5
                    p[1] *= 0.25
                    p[0] = p[0]*math.cos(rot) + p[1]*math.sin(rot)
                    p[1] = - p[0]*math.sin(rot) + p[1]*math.cos(rot)
                    #p[0] += int(0.25*r.raster_dimensions[0])
                    p[0] -= 50
                    p[1] += 250
                    pass

                other = gdalutils.project_raster_to_geo(
                    points=other,
                    affine_trans=r.affine_trans)

                other = gdalutils.points2polygon(other)
                logging.debug('OTHER: {}'.format(other))

                logging.info('Started clipping raster...')
                result = gdalutils.clip_dataset(r.dataset, other, ogr_driver_name=ogr_driver_name)
                assert result is not None
                logging.info('Finished clipping raster.')

                logging.info('Started creating copy of clipped raster...')
                gdal_driver_name = 'GTiff'
                gdal_driver = gdal.GetDriverByName(gdal_driver_name)
                result_final = gdal_driver.CreateCopy(
                    utf8_path=os.path.join(self.test_output_path, geotiff_file),
                    src=result, strict=0, options=['COMPRESS=LZW'])
                logging.info('Finished creating copy of clipped raster.')
                assert result_final is not None
                result = None
                result_final = None
                assert result is None
                assert result_final is None
                logging.info('Finished clipping for file: "{}" .'.format(geotiff_file))

        except Exception as e:
            logging.error('Error: {}'.format(e))
            traceback.print_exc(file=sys.stdout)
            raise e

class TestGdalrdf(unittest.TestCase):
    """Test the gdalfrdf module."""

    def test_create_graph_from_iri(self):
        tmp_file = tempfile.NamedTemporaryFile(delete=True)
        dataset = gdalutils.create_dataset_random()
        gdal_driver_name = 'GTiff'
        gdal_driver = gdal.GetDriverByName(gdal_driver_name)
        dataset = gdal_driver.CreateCopy(tmp_file.name, dataset, 0)
        assert dataset is not None
        dataset = None
        assert dataset is None

        raster_iri = 'http://example.com/raster/'
        raster_file = 'file:{}'.format(tmp_file.name)
        r = Raster(dataset=create_raster_from_iri(iri=raster_file), raster_iri=raster_iri)
        self.assertEqual(URIRef(raster_iri), r.iri)

    def test_create_graph(self):
        raster_iri = 'http://example.com/raster/'
        g = Raster(
            dataset=gdalutils.create_dataset_zeros(),
            raster_iri=raster_iri)
        assert g is not None

    def test_get_pixel_value(self):
        raster_iri = 'http://example.com/raster/'
        g = Raster(
            dataset=gdalutils.create_dataset_zeros(),
            raster_iri=raster_iri)

        # The srs of points for which we check their pixel value is WGS84.
        spatial_ref_point = osr.SpatialReference()
        spatial_ref_point.SetWellKnownGeogCS('WGS84')

        # We will check the points of the bounding box
        geo_points = g.rasterbb_to_geo()
        logging.debug('BB point {}'.format(geo_points[0]))
        logging.debug('BB point {}'.format(geo_points[1]))
        logging.debug('BB point {}'.format(geo_points[2]))
        logging.debug('BB point {}'.format(geo_points[3]))

        for gp in geo_points:
            self.assertTrue(gdalutils.point_in_raster(
                point=gp,
                affine_trans_inv=g.affine_trans_inv,
                raster_dimensions=g.raster_dimensions))

            # Check their pixel value
            pixel_value = get_pixel_value(
                dataset=g.dataset,
                band_no=1,
                point=gp,
                affine_trans_inv=g.affine_trans_inv,
                raster_dimensions=g.raster_dimensions,
                spatial_ref_raster=g.spatial_ref,
                spatial_ref_point=g.spatial_ref)

            self.assertTrue(pixel_value is not None)
            self.assertTrue(pixel_value.value == 0)

            logging.info('Pixel value RDF Literal: {}'.format(pixel_value.n3()))

if __name__ == '__main__':
    unittest.main()
    sys.exit(0)
