#!/usr/bin/env python
# coding:utf-8

"""lrdgdal tests for the sparql module"""

__author__ = "Thomas Scharrenbach (thomas@scharrenbach.net)"
__copyright__ = "Copyright (C) 2014 Thomas Scharrenbach"
__license__ = "Apache License v2"
__version__ = "0.0.4"

import sys

import traceback

import os

import math

import codecs

import unittest

import logging

import logging.config

from rdflib import *

try:
    from osgeo import gdal, osr, ogr
except ImportError:
    import gdal
    import osr
    import ogr

from lrdgdal import *

from lrdgdal.namespace import GDAL, GEOSPARQL

from tests.testutils import TestLRDGDAL


logging.config.fileConfig('tests/logging.conf')


class TestOpClip(TestLRDGDAL):
    """Test clipping."""

    @classmethod
    def setUpClass(cls):
        cls.test_output = 'clip_dataset_sparql'
        cls.init_geotiff_files()

    def test_clip(self):
        ogr_driver_name = 'Memory'
        sparql.activate(modules=[GDAL.clip])

        try:
            for root, geotiff_file in self.geotiff_files:
                g = RasterRDFDataset()
                src_ds = create_raster_from_file(filename=os.path.join(root, geotiff_file))

                logging.info('Started clipping for file: "{}" ...'.format(geotiff_file))

                raster_iri = 'http://example.com/raster/'
                r = Raster(
                    dataset=src_ds,
                    raster_iri=raster_iri)
                g.add_raster(r)

                other = r.bbox
                logging.debug(other)
                rot = math.radians(5)
                for p in other:
                    #p[0] = p[0] * 0.5 + p[1] * 0.5
                    #p[1] = p[0] * 0.5 + p[1] * 0.5
                    p[0] *= 0.5
                    p[1] *= 0.25
                    p[0] = p[0]*math.cos(rot) + p[1]*math.sin(rot)
                    p[1] = - p[0]*math.sin(rot) + p[1]*math.cos(rot)
                    #p[0] += int(0.25*r.raster_dimensions[0])
                    p[0] -= 50
                    p[1] += 250
                    pass

                other = gdalutils.project_raster_to_geo(
                    points=other,
                    affine_trans=r.affine_trans)

                other = gdalutils.points2polygon(other)
                mask_geometry_literal_value = ogcutils.encode_as_wktliteral(
                    spatial_ref=osr.SpatialReference(r.dataset.GetProjection()), geometry=other)
                mask_geometry_literal = Literal(mask_geometry_literal_value, datatype=GEOSPARQL.WKT_LITERAL)
                logging.debug('OTHER: {}'.format(other))

                logging.info('Started clipping raster...')
                query = \
                    'PREFIX gdal: <{prefix_gdal}>\n' \
                    'PREFIX rdf: <{prefix_rdf}>\n' \
                    'SELECT *  WHERE {{\n' \
                    '{{\n' \
                    '  ?band a gdal:Band .\n' \
                    '}}\n' \
                    'Graph ?g {{\n' \
                    '  ?band gdal:clipValue ?clip_val .\n' \
                    '  ?clip_val gdal:clip ?clip .\n' \
                    '  ?clip_val gdal:maskGeometry {mask_geometry} .\n' \
                    '}}\n' \
                    '}}'\
                    .format(
                        prefix_gdal=GDAL.NS,
                        prefix_rdf=RDF.uri,
                        mask_geometry=mask_geometry_literal.n3())

                logging.info(query)
                for x in g.query(query):
                    result = codecs.decode(x['clip'].value, 'hex_codec')
                    with open(os.path.join(self.test_output_path, geotiff_file), 'w+b') as result_file:
                        result_file.write(result)
                    logging.info('Writing result to disk...')

                logging.info('- - - - - - - - - - - - - - -')
                logging.info('Finished clipping raster.')

                g = None

        except Exception as e:
            logging.error('Error: {}'.format(e))
            traceback.print_exc(file=sys.stdout)
            raise e


class TestLrdgdalSparql(unittest.TestCase):
    """Testing the SPARQL capabilites."""

    def test_sparql_bgp(self):
        sparql.activate(modules=[GDAL.band_value])

        g = RasterRDFDataset()

        raster_iri = 'http://example.com/raster/'

        dataset = gdalutils.create_dataset_random()

        raster = Raster(
            dataset=dataset,
            raster_iri=raster_iri)

        g.add_raster(raster=raster)

        bb_geo = raster.rasterbb_to_geo(spatial_ref_target=gdalutils.WGS84)

        point = bb_geo[0]

        point = 'POINT( {} {} )'.format(point[0], point[1])

        point = Literal(
            ogcutils.encode_as_wktliteral(
                spatial_ref=gdalutils.WGS84,
                geometry=point),
            datatype=GEOSPARQL.WKT_LITERAL)

        for x in g.query('PREFIX gdal: <{prefix_gdal}> SELECT * WHERE {{?s a gdal:Band}}'.format(prefix_gdal=GDAL.NS)):
            logging.info(x)

        query = \
            'PREFIX gdal: <{prefix_gdal}>\n' \
            'PREFIX rdf: <{prefix_rdf}>\n' \
            'SELECT *  WHERE {{\n' \
            'GRAPH ?g {{\n' \
            '  ?band a gdal:Band .\n' \
            '  ?band gdal:bandValue ?band_val .\n' \
            '  ?band_val gdal:pixelCoordinates {point}.\n' \
            '  ?band_val gdal:pixelValue ?pixelValue .\n' \
            '}}\n' \
            '}}'\
            .format(
                prefix_gdal=GDAL.NS,
                prefix_rdf=RDF.uri,
                point=point.n3())

        query = \
            'PREFIX gdal: <{prefix_gdal}>\n' \
            'PREFIX rdf: <{prefix_rdf}>\n' \
            'SELECT *  WHERE {{\n' \
            '{{\n' \
            '  ?band a gdal:Band .\n' \
            '}}\n' \
            'Graph ?g {{\n' \
            '  ?band gdal:bandValue ?band_val .\n' \
            '  ?band_val gdal:pixelCoordinates {point}.\n' \
            '  ?band_val gdal:pixelValue ?pixelValue .\n' \
            '}}\n' \
            '}}'\
            .format(
                prefix_gdal=GDAL.NS,
                prefix_rdf=RDF.uri,
                point=point.n3())

        logging.info(query)
        for x in g.query(query):
            logging.info('Result: {}'.format(x))

        logging.info('- - - - - - - - - - - - - - -')


if __name__ == '__main__':
    logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)
    logging.getLogger("root").setLevel(logging.DEBUG)
    unittest.main()

    sys.exit(0)
