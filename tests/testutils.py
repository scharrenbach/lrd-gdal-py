#!/usr/bin/env python
# coding:utf-8

"""lrdgdal test utilities"""

__author__ = "Thomas Scharrenbach (thomas@scharrenbach.net)"
__copyright__ = "Copyright (C) 2014 Thomas Scharrenbach"
__license__ = "Apache License v2"
__version__ = "0.0.4"

import os

import logging

import unittest

import warnings

try:
    from osgeo import gdal, osr, ogr
except ImportError:
    import gdal
    import osr
    import ogr

from lrdgdal import gdalutils


logging.config.fileConfig('tests/logging.conf')

class TestLRDGDAL(unittest.TestCase):
    """General test class for testing things for the GeoTIFF files in test/data."""

    @classmethod
    def init_geotiff_files(self):
        """Scan for GeoTIFF files in test/data.

        """
        self.geotiff_files = list()
        for root, dirs, files in os.walk('tests/data'):
            for file in files:
                if file.endswith(".tif"):
                    self.geotiff_files.append([root, file])
        assert len(self.geotiff_files) > 0

        assert self.test_output is not None

        self.test_output_path = os.path.join('test-output', self.test_output, '')
        d = os.path.dirname(self.test_output_path)
        if not os.path.exists(d):
            logging.info(
                'Creating test-output dir for "{}": "{}"'.format(self.test_output, self.test_output_path))
            os.makedirs(d)
        else:
            logging.info(
                'Test-output dir for "{}" already exists: "{}"'.format(self.test_output, self.test_output_path))



def _create_raster_base(driver="GTiff", raster_x_size=512, raster_y_size=512):
    warnings.warn('Deprecated! Please use lrdgdal.gdalutils.create_raster_base instead!', DeprecationWarning)
    return gdalutils.create_raster_base(driver=driver, raster_x_size=raster_x_size, raster_y_size=raster_y_size)


def create_dataset_random():
    warnings.warn('Deprecated! Please use lrdgdal.gdalutils.create_dataset_random instead!', DeprecationWarning)
    return gdalutils.create_dataset_random()


def create_dataset_zeros():
    warnings.warn('Deprecated! Please use lrdgdal.gdalutils.create_dataset_zeros instead!', DeprecationWarning)
    return gdalutils.create_dataset_zeros()