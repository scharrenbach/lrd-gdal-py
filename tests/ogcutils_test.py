#!/usr/bin/env python
# coding:utf-8

"""lrdgdal tests for the ogcutils module"""

__author__ = "Thomas Scharrenbach (thomas@scharrenbach.net)"
__copyright__ = "Copyright (C) 2014 Thomas Scharrenbach"
__license__ = "Apache License v2"
__version__ = "0.0.4"


import unittest

try:
    from osgeo import osr
    from osgeo import ogr
except ImportError:
    import osr
    import ogr

from lrdgdal import ogcutils


class TestLrdgdalSparql(unittest.TestCase):
    def test_parse_wktliteral(self):
        point = '<http://www.opengis.net/def/crs/OGC/1.3/CRS84> Point( -83.38 33.95)'
        print(point)
        [spatial_ref, wkt_point] = ogcutils.parse_wktliteral(point)
        print(str(spatial_ref.ExportToXML()))
        print(str(wkt_point))

    def test_encode_as_wktliteral(self):
        spatial_ref = osr.SpatialReference()
        spatial_ref.SetWellKnownGeogCS("WGS84")
        iri = ogcutils.extract_iri_from_srs(spatial_ref)
        print("IRI: " + str(iri))
        wkt_geometry = 'Point( -83.38 33.95)'
        geometry = ogr.CreateGeometryFromWkt(wkt_geometry)
        wkt_literal = ogcutils.encode_as_wktliteral(spatial_ref, geometry)
        print("Literal: " + str(wkt_literal))

if __name__ == '__main__':
    unittest.main()

    import sys
    sys.exit(0)
